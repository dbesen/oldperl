#!/usr/bin/perl -w

use Net::IRC;
use Lingua::Ispell qw( spellcheck );
Lingua::Ispell::allow_compounds(1);
$Lingua::Ispell::path='/usr/bin/ispell';
use Lingua::Ispell qw( add_word_lc );
use Lingua::Ispell qw( save_dictionary );

#system("rm /tmp/delme");
#system("mkfifo /tmp/delme");

my $chan;
$chan = "#linux";

$irc = new Net::IRC;

$conn = $irc->newconn(Nick => 'bot', Server=>'216.17.145.185', Ircname=>'bot');

$conn->add_global_handler('376', \&on_connect);
$conn->add_global_handler('public', \&on_public);
$conn->add_handler('msg', \&on_msg);

print ("....\n");
#$irc->start;


sub on_connect {
    my $tmp;
    my $self = shift;
    $self->join ($chan);
    $self->print ("joined\n");
}

sub on_msg {
    my ($self, $event) = @_;
    my ($nick, $arg) = ($event->nick, join(' ', $event->args));

    print "$arg : blah blah\n";
    if($arg =~ /join/) {$self->join ($chan);}
    elsif($arg =~ /^\s*spell\s+(.*)$/) {spell($self,$nick,$1);}
    else {$self->nick($arg);}
}

sub on_public {
    my ($self, $event) = @_;
    my ($arg) = $event->args;
    my @temp = split(/ /, $arg);


}
sub spell {
    print "spell called\n";
    my ($self, $nick, $arg) = @_;
    my @in = split(/ /, $arg);
    #shift @in; # 1st arg is 'spell'..
    if($in[0] eq "add") {add_word_lc($in[1]);save_dictionary();$self->privmsg($nick, "$in[1] added");return;}
    for my $r ( spellcheck( $arg ) ) {
        if ( $r->{'type'} eq 'ok' ) {
            $self->privmsg($nick, "'$r->('term'} was found in the dictionary.\n");
            return;
        }
        elsif ( $r->{'type'} eq 'root' ) {
            $self->privmsg($nick, "'$r->{'term'}' can be formed from root '$r->{'root'}'\n");
            return;
        }
        elsif ( $r->{'type'} eq 'miss' ) {
            $self->privmsg($nick, "'$r->{'term'}' was not found in the dictionary.\n");
            $self->privmsg($nick, "Near misses: @{$r->{'misses'}}\n");
            return;
        }
        elsif ( $r->{'type'} eq 'guess' ) {
            $self->privmsg($nick, "'$r->{'term'} was not found in the dictionary.\n");
            $self->privmsg($nick, "Root/affix Guesses: @($r->{'guesses'})\n");
            return;
        }
        elsif ( $r->{'type'} eq 'compound' ) {
            $self->privmsg($nick, "'$r->{'term'} is a valid compound word\n");
            return;
        }
        elsif ( $r->{'type'} eq 'none' ) {
            $self->privmsg($nick, "No match for term '$r->{'term'}'\n");
            return;
        }
    }
}
$irc->do_one_loop();
$conn->join($chan);
$irc->do_one_loop();



while(1) { # urlpaster is basic functionality...
    sleep 1;
    $irc->do_one_loop();
    if (-s "/home/sargon/urls") {
        open FIFO, "+< /home/sargon/urls";
        while (my $tmp = <FIFO>) {
            $conn->privmsg($chan, $tmp);
            $irc->do_one_loop();
        }
        close FIFO;
    }
}


