#!/usr/bin/perl
use warnings;
open (FILE, "blah.txt");
open (NEWFILE, ">> new.txt");
while (<FILE>) {
	$blah=$_;
	chomp($blah);
	print "blah is $blah\n";
	if (length($blah) <= 2) {
		print NEWFILE "$blah\n";
	}
}
close(NEWFILE);
close(FILE);
