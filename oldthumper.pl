#!/usr/bin/perl -w

use Net::IRC;

my @keywords; # yes, I know I could use a hash for this, but I'm a c programmer. Deal.
my @sayings;  #
my $sayN;
my $chan;
$chan = "#linux";

$irc = new Net::IRC;

$conn = $irc->newconn(Nick => 'thumper', Server=>'216.17.145.185', Ircname=>'thumper');

$conn->add_global_handler('376', \&on_connect);
$conn->add_global_handler('public', \&on_public);
$conn->add_handler('msg', \&on_msg);

$sayN = 0;

print ("....\n");
$irc->start;

sub on_connect {
    my $self = shift;
    $self->join ($chan);
    $self->print ("joined\n");
}

sub on_msg {
    my ($self, $event) = @_;
    my ($arg) = $event->args;
    my @in = split(/ /, $arg);
    if($in[0] eq "join") {$self->join ($chan);}
    else {
        if($event->nick eq "sargon") {$self->privmsg($chan, "*$arg* *$arg* *$arg*")};
    }
}

sub on_public {
    my ($self, $event) = @_;
    my ($arg) = $event->args;
    my @temp = split(/ /, $arg);
    my $tmp = $temp[int rand $#temp];
    if($arg =~ s/\*([^\*]+)\* \*//g) {
        #if(substr($event->nick, 0, 5) eq "golgo") {$self->privmsg($chan, "I won't thump for golgo."); return;}
        #if(lc(substr($event->nick, 0, 5)) eq "jesse") {$self->privmsg($chan, "I won't thump for Jesse."); return;}
        $self->privmsg($chan, "*$1* *$1* *$1*");
        return;
    }
    if(int rand(50) > 0) {return;}
    if(length($tmp) <= 3) {return;}
    $tmp =~ s/[^a-zA-Z0-9]//g;
    $self->privmsg($chan, "*$tmp* *$tmp* *$tmp*");
}
