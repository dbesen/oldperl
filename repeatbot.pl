#!/usr/bin/perl -w

use Net::IRC;
use strict;

my %sayings;

my $key;
my $say;

my $chan;
$chan = "#linux";

my $irc = new Net::IRC;

my $conn = $irc->newconn(Nick => 'repeatbot', Server=>'216.17.145.185', Ircname=>'repeatbot');

$conn->add_global_handler('376', \&on_connect);
$conn->add_global_handler('public', \&on_public);
$conn->add_handler('msg', \&on_msg);

print ("....\n");
$irc->start;

sub on_connect {
    my $self = shift;
    $self->join ($chan);
    $self->print ("joined\n");
}

sub on_msg {
    my ($self, $event) = @_;
    my ($arg) = $event->args;
    my @in = split(/ /, $arg);
    if($in[0] eq "join") {$self->join ($chan);}
    elsif(lc($in[0]) eq "repeat") {
        shift @in;
        $key = $in[0];
        shift @in;
        $say = join(" ", @in);
        $sayings{$key} = $say;
        $self->privmsg($event->nick, "repeating '$say' when someone says '$key'");
    }
    elsif(lc($in[0]) eq "clear") {
        undef %sayings;
        $self->privmsg($event->nick, "cleared");
    }
    elsif(lc($in[0]) eq "help") {
        $self->privmsg($event->nick, "/msg repeatbot repeat <keyword> <saying>");
        $self->privmsg($event->nick, "/msg repeatbot clear");
    }
}

sub on_public {
    my ($self, $event) = @_;
    my ($arg) = $event->args;
    my @temp = split(/ /, $arg);

    foreach $key (keys %sayings) {
        if($arg =~ $key) {
            $self->privmsg($chan, $sayings{$key});
        }
    }
 
}
