#!/usr/bin/perl -w

use Net::IRC;
use strict;

my $chan;
$chan = "#test";

my $irc = new Net::IRC;

my $conn = $irc->newconn(Nick => 'dccbot', Server=>'216.17.145.185', Ircname=>'dccbot');

$conn->add_global_handler('376', \&on_connect);
$conn->add_global_handler('public', \&on_public);
$conn->add_handler('chat', \&on_chat);
$conn->add_handler('msg', \&on_msg);


print ("....\n");
$irc->start;

sub on_msg {
    my ($self, $event) = @_;
    my ($arg) = $event->args;
    if($arg eq 'join') { $self->join($chan);}
}

sub on_connect {
    my $self = shift;
    $self->join ($chan);
    $self->print ("joined\n");
}

sub on_public {
    my ($self, $event) = @_;
    my ($arg) = $event->args;
    if($arg eq "chatme") {
        $self->new_chat(1, $event->nick, $event->host);
    }
}

sub on_chat {
    my ($self, $event) = @_;
    my ($sock) = ($event->to)[0];
    $self->privmsg($sock, "boo");
}
