#!/usr/local/bin/perl -w
use strict;
use Net::IRC;

#
#  Create the IRC and Connection objects
#

my $irc = new Net::IRC;

print "Creating connection to IRC server...\n";

my $conn = $irc->newconn(Server   => ($ARGV[0]  ||  '216.17.145.185'),
			 Port     => 6667,
			 Nick     => 'HarlockBot',
			 Ircname  => 'Craig Russel',
			 Username => 'Phatty')
    or die "irctest: Can't connect to IRC server.\n";


# Handles some messages you get when you connect
sub on_init {
    my ($self, $event) = @_;
    my (@args) = ($event->args);
    shift (@args);
    $self->join("#linux");
    print "*** @args\n";
}


# What to do when we receive a private PRIVMSG.
sub on_msg {
    my ($self, $event) = @_;
    my ($nick) = $event->nick;

    print "*$nick*  ", ($event->args), "\n";
    $self->privmsg($event->nick,"Go AWAY biotch!"); #say  
}

sub on_join {
    my ($self, $event) = @_;
    my ($channel) = ($event->to)[0];

    $event->nick, $event->userhost, $channel; 
}

sub on_public {
    srand();
    my ($self, $event) = @_;
    my ($arg) = ($event->args);
    my ($nick, $mynick) = ($event->nick, $self->nick);
    chomp ($arg);
    my @words = split (/ +/, $arg);    
    my $namezor=$words[0];
    print $namezor;
    my @quotes = ("Phatty bitch!", "suck my nut", "scratchy, scratchy", 
               "cock weasel", "cock whore", "niggah please", 
               "where's my Snoop Dog CD?");

   my @answers = ("I don't know biotch!", "Dude, your questions confuse me",
                  "Suck my nut cock weasel, I dunno!", "Why are you asking me?",
                  "Do battleships have screen doors?  I don't know!", 
                  "phatty niggah Russel doesn't know!");
    
    if (int rand(30)==1)
      {
	my $quote = $quotes[int rand $#quotes];
        $self->privmsg("#linux", $quote);
      }

    print $namezor, ":", $arg, "\n";
        
    if (($namezor =~ /$mynick/i) && ($arg =~ /\?/)) 
      {
        my $ans = $answers[int rand $#answers];
        my $text=$nick . ": "; 
        $text .= $ans;
        $self->privmsg ("#linux",$text);
      }
      
}

print "Installing handler routines...";

#$conn->add_handler('cping',  \&on_ping);
#$conn->add_handler('crping', \&on_ping_reply);
$conn->add_handler('msg',    \&on_msg);
#$conn->add_handler('chat',   \&on_chat);
$conn->add_handler('public', \&on_public);
#$conn->add_handler('caction', \&on_action);
$conn->add_handler('join',   \&on_join);
#$conn->add_handler('part',   \&on_part);
#$conn->add_handler('cdcc',   \&on_dcc);
#$conn->add_handler('topic',   \&on_topic);
#$conn->add_handler('notopic',   \&on_topic);

$conn->add_global_handler([ 251,252,253,254,302,255 ], \&on_init);
$conn->add_global_handler('disconnect', \&on_disconnect);
$conn->add_global_handler(376, \&on_connect);
#$conn->add_global_handler(433, \&on_nick_taken);
#$conn->add_global_handler(353, \&on_names);

print " done.\n";

print "starting...\n";
$irc->start;
