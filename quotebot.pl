#!/usr/bin/perl -w

use Net::IRC;
use strict;

my $chan;
$chan = "#bots";

my $irc = new Net::IRC;

my $conn = $irc->newconn(Nick => 'quotebot', Server=>'216.17.145.185', Ircname=>'quotebot');

$conn->add_global_handler('376', \&on_connect);
$conn->add_global_handler('public', \&on_public);
$conn->add_handler('msg', \&on_msg);


print ("....\n");
$irc->start;

sub on_connect {
    my $self = shift;
    $self->join ($chan);
    $self->print ("joined\n");
}

sub on_msg {
    my ($self, $event) = @_;
    my ($arg) = $event->args;
    my @in = split(/ /, $arg);
    if($in[0] eq "join") {$self->join ($chan);}
}

sub on_public {
    my ($self, $event) = @_;
    my ($arg) = $event->args;
    my @in = split(/ /, $arg);

    if($in[0] eq "!quote") {

        my @ary;
        if ($in[1]) {
            shift @in;
            my $tmp = join(" ", @in);
            $tmp =~ s/\`//g;
            $tmp =~ s/\;//g;
            open FL, "grep -r -h -i '$tmp' /home/talon/eggdrop/mel/logs/*linux*|grep -v '\!quote'|grep -v 'quotebot'|"; @ary = <FL>; $self->privmsg($chan, $ary[int rand $#ary]);
        }
        else {
            open FL, "grep -r -h '' /home/talon/eggdrop/mel/logs/*linux*|grep -v '\!quote'|grep -v 'quotebot'|"; @ary = <FL>; $self->privmsg($chan, $ary[int rand $#ary]);
        }
close FL;
    }


}
