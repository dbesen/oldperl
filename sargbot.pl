#!/usr/bin/perl -w

use Net::IRC;

my $chan;
$chan = "#linux";

$irc = new Net::IRC;

$conn = $irc->newconn(Nick => 'sargbot', Server=>'216.17.145.185', Ircname=>'sargbot');

$conn->add_global_handler('376', \&on_connect);
$conn->add_global_handler('public', \&on_public);
$conn->add_handler('msg', \&on_msg);

print ("....\n");
$irc->start;

sub on_connect {
    my $self = shift;
    $self->join ($chan);
    $self->print ("joined\n");
}

sub on_msg {
    my ($self, $event) = @_;
    my ($arg) = $event->args;
    my @in = split(/ /, $arg);
    if($in[0] eq "join") {$self->join ($chan);}
    else {
        if($event->nick eq "sargon") {$self->privmsg($chan, "*$arg* *$arg* *$arg*")};
    }
}

sub on_public {
    my ($self, $event) = @_;
    my ($arg) = $event->args;
    $arg =~ s/[^a-zA-Z0-9 ]//g;
    my @temp = split(/ /, $arg);
    my $tmp = $temp[int rand $#temp];
    if(int rand(50) > 0) {return;}
    if(length($tmp) <= 3) {return;}
    $self->privmsg($chan, "I should write a $tmp"."bot");
}
