#!/usr/bin/perl -w

use Net::IRC;

my @keywords; # yes, I know I could use a hash for this, but I'm a c programmer. Deal.
my @sayings;  #
my $sayN;
my $chan;
$chan = "#linux";

$irc = new Net::IRC;

$conn = $irc->newconn(Nick => 'tachshadow', Server=>'216.17.145.185', Ircname=>'tachshadow');

$conn->add_global_handler('376', \&on_connect);
$conn->add_global_handler('public', \&on_public);
$conn->add_handler('msg', \&on_msg);

$sayN = 0;

print ("....\n");
$irc->start;

sub on_connect {
    my $self = shift;
    $self->join ($chan);
    $self->print ("joined\n");
}

sub on_msg {
    my ($self, $event) = @_;
    my ($arg) = $event->args;
    my @in = split(/ /, $arg);
    if($in[0] eq "join") {$self->join ($chan);}
}

sub on_public {
    my ($self, $event) = @_;
    my ($arg) = $event->args;
    my ($nick) = $event->nick;
    if($nick eq "Tachyon") {$self->privmsg("Tachyon", $arg);}
}
