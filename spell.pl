#!/usr/bin/perl -w

use Net::IRC;
use Lingua::Ispell qw( spellcheck );
Lingua::Ispell::allow_compounds(1);
$Lingua::Ispell::path='/usr/bin/ispell';
use Lingua::Ispell qw( add_word_lc );
use Lingua::Ispell qw( save_dictionary );
use strict;

my $chan;
$chan = "#linux";

my $irc = new Net::IRC;

my $conn = $irc->newconn(Nick => 'Spellbot', Server=>'216.17.145.185', Ircname=>'Spellbot');

$conn->add_global_handler('376', \&on_connect);
$conn->add_global_handler('public', \&on_public);
$conn->add_handler('msg', \&on_msg);


print ("....\n");
$irc->start;

sub on_connect {
    my $self = shift;
    $self->join ($chan);
    $self->print ("joined\n");
}

sub on_msg {
    my ($self, $event) = @_;
    my ($arg) = $event->args;
    my @in = split(/ /, $arg);
    my $nick = $event->nick;
    if($in[0] eq "join") {$self->join ($chan); return;}
    if($in[0] eq "add") {add_word_lc($in[1]);save_dictionary();$self->privmsg($nick, "$in[1] added");return;}
    for my $r ( spellcheck( $arg ) ) {
        if ( $r->{'type'} eq 'ok' ) {
            $self->privmsg($nick, "'$r->('term'} was found in the dictionary.\n");
            return;
        }
        elsif ( $r->{'type'} eq 'root' ) {
            $self->privmsg($nick, "'$r->{'term'}' can be formed from root '$r->{'root'}'\n");
            return;
        }
        elsif ( $r->{'type'} eq 'miss' ) {
            $self->privmsg($nick, "'$r->{'term'}' was not found in the dictionary.\n");
            $self->privmsg($nick, "Near misses: @{$r->{'misses'}}\n");
            return;
        }
        elsif ( $r->{'type'} eq 'guess' ) {
            $self->privmsg($nick, "'$r->{'term'} was not found in the dictionary.\n");
            $self->privmsg($nick, "Root/affix Guesses: @($r->{'guesses'})\n");
            return;
        }
        elsif ( $r->{'type'} eq 'compound' ) {
            $self->privmsg($nick, "'$r->{'term'} is a valid compound word\n");
            return;
        }
        elsif ( $r->{'type'} eq 'none' ) {
            $self->privmsg($nick, "No match for term '$r->{'term'}'\n");
            return;
        }
    }
}

sub on_public {
    return; # too annoying
    my ($self, $event) = @_;
    my ($arg) = $event->args;
    my $nick = $event->nick;
    my @temp = split(/ /, $arg);

    if($nick =~ /polack/i) {return;}
    # for now
    $chan = $nick;
    for my $r ( spellcheck( $arg ) ) {
        if ( $r->{'type'} eq 'ok' ) {
            #print "'$r->('term'} was found in the dictionary.\n";
        }
        elsif ( $r->{'type'} eq 'root' ) {
            #print "'$r->{'term'}' can be formed from root '$r->{'root'}'\n";
        }
        elsif ( $r->{'type'} eq 'miss' ) {
            $self->privmsg($chan, "$nick: $r->{'term'}: Near misses: @{$r->{'misses'}}\n");
        }
        elsif ( $r->{'type'} eq 'guess' ) {
            $self->privmsg($chan, "$nick: $r->{'term'}: Root/affix Guesses: @($r->{'guesses'})\n");
        }
        elsif ( $r->{'type'} eq 'compound' ) {
            #print "'$r->{'term'} is a valid compound word\n";
        }
        elsif ( $r->{'type'} eq 'none' ) {
            $self->privmsg($chan, "No match for term '$r->{'term'}'\n");
        }
    }

}
