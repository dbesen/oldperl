#!/usr/bin/perl -w

use Net::IRC;
use strict;

my $chan;
$chan = "#linux";

my $irc = new Net::IRC;

my $conn = $irc->newconn(Nick => 'rambot', Server=>'216.17.145.185', Ircname=>'rambot');

$conn->add_global_handler('376', \&on_connect);
$conn->add_global_handler('public', \&on_public);
$conn->add_handler('msg', \&on_msg);
$conn->add_handler('join', \&on_join);


print ("....\n");
$irc->start;

sub on_connect {
    my $self = shift;
    $self->join ($chan);
    $self->print ("joined\n");
}

sub on_msg {
    my ($self, $event) = @_;
    my ($arg) = $event->args;
    my @in = split(/ /, $arg);
    if($in[0] eq "join") {$self->join ($chan);$self->privmsg("nickserv", "id ramconrules");}
    print $arg, "\n";
}

sub on_public {
    my ($self, $event) = @_;
    my ($arg) = $event->args;
}

sub on_join {
    my ($self, $event) = @_;

    if($event->nick =~ /sarek/i) {return;}

    $self->privmsg("DarkHelmet", "invite #ramcon ".$event->nick);
    print("DarkHelmet", "invite #ramcon ".$event->nick);
    print "\n";
}
