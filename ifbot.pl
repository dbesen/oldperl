#!/usr/bin/perl -w

use Net::IRC;

my $chan;
$chan = "#linux";

$irc = new Net::IRC;

$conn = $irc->newconn(Nick => 'ifBot', Server=>'216.17.145.185', Ircname=>'ifBot');

$conn->add_global_handler('376', \&on_connect);
$conn->add_global_handler('public', \&on_public);
$conn->add_handler('msg', \&on_msg);

print ("....\n");
$irc->start;

sub on_connect {
    my $self = shift;
    $self->join ($chan);
    $self->print ("joined\n");
}

sub on_msg {
    my ($self, $event) = @_;
    my ($arg) = $event->args;
    my @in = split(/ /, $arg);
    if($in[0] eq "join") {$self->join ($chan);return;}
    if($event->nick eq "sargon") {$self->privmsg($chan, $arg);}
}

sub on_public {
    my ($self, $event) = @_;
    my ($arg) = $event->args;
    my $nick = $event->nick;
    my $ret = "$nick: what?";
    my $talk = "no";
    if (lc($arg) eq "hi ifbot") {
        $ret = "hi $nick";
        $talk = "yes";
    }
    if (lc($arg) eq "yo ifbot") {
        $ret = "hi $nick";
        $talk = "yes";
    }
    if (lc($arg) eq "hello ifbot") {
        $ret = "hi $nick";
        $talk = "yes";
    }
    if(substr(lc($arg),0, 5) eq "ifbot") {
        $talk = "yes";
        $arg =~ s/[^a-zA-Z0-9\?\:\'\.\, ]//g;
        $arg = substr($arg, 7);
        if($arg =~ /\?/) {
            $ret = "$nick: I don't know what you're talking about";
            if($arg =~ / a bot/i) {
                $ret = "I'm not just a bot, I'm a complicated jungle of spaghetti logic";
            }
        }
        if($arg =~ /don\'t you\?/i) {
            $ret = "$nick: no, I don't";
        }
        if($arg =~ /don\'t you\./i) {
            $ret = "$nick: no, I don't";
        }
        if($arg =~ /^do you/i) {
            $ret = "$nick: no, I don't";
        }
        if($arg =~ /^what\'s up/i) {
            $ret = "$nick: nothing, you?";
        }
        if($arg eq "die") {
            $talk = "no";
            $self->me($chan, "dies");
        }
    }
    if($talk ne "no") {
        sleep ((length $ret) / 15);
        $self->privmsg($chan, $ret);
    }
}
